/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeproject;

/**
 *
 * @author ASUS
 */
public class TestRectangle {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4, 3);
        System.out.println("Area of Rectangle (w = " + rectangle1.getW() + "," + "h = " + rectangle1.getH() + ") is " + rectangle1.calArea());
        rectangle1.setH(0);
        System.out.println("Area of Rectangle (w = " + rectangle1.getW() + "," + "h = " + rectangle1.getH() + ")is " + rectangle1.calArea());
        rectangle1.setW(0);
        System.out.println("Area of Rectangle (w = " + rectangle1.getW() + "," + "h = " + rectangle1.getH() + ")is " + rectangle1.calArea());
        System.out.println(rectangle1.toString());
    }
}
