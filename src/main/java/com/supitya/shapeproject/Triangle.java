/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeproject;

/**
 *
 * @author ASUS
 */
public class Triangle {
    private double b,h;
    public Triangle(double b,double h){
        this.b = b ;
        this.h = h ;
    }
    public double calArea() {
        return (b * h)/2;
    }
    public double getB() {
        return b;
    }
    public double getH() {
        return h;
    }
    public void setB(double b) {
        if (b <= 0) {
            System.out.println("Error: base must more than zero!!!!");
            return;
        }
        this.b = b;
    }

    public void setH(double h) {
        if (h <= 0) {
            System.out.println("Error: hight must more than zero!!!!");
            return;
        }
        this.h = h;
    }
}
