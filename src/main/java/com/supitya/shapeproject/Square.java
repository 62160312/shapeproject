/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeproject;

/**
 *
 * @author ASUS
 */
public class Square {

    private double n;

    public Square(double n) {
        this.n = n;

    }

    public double calArea() {
        return n * n;
    }

    public double getR() {
        return n;
    }

    public void setR(double n) {
        if (n <= 0) {
            System.out.println("Error: side must more than zero!!!!");
            return;
        }
        this.n = n;
    }
    @Override
    public String toString(){
        return "Area of square ( n = "+this.getR() +")is "+ this.calArea();
    }
}
