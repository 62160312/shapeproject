/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeproject;

/**
 *
 * @author ASUS
 */
public class TestSquare {

    public static void main(String[] args) {
        Square square1 = new Square(2);
        System.out.println("Area of Square ( n = " + square1.getR() + ") is " + square1.calArea());
        square1.setR(3);
        System.out.println("Area of Square ( n = " + square1.getR() + ") is " + square1.calArea());
        square1.setR(0);
        System.out.println("Area of Square ( n = " + square1.getR() + ") is " + square1.calArea());
        System.out.println(square1.toString());
        System.out.println(square1);
    }

}
